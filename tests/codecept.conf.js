exports.config = {
    output: './output',
    helpers: {
        Puppeteer: {
            url: 'http://localhost:3010',
            show: !process.env.CI,
            headless: !!process.env.CI,
            windowSize: "1024x768",
        }
    },
    include: {
        I: './steps_file.js'
    },
    mocha: {},
    bootstrap: null,
    teardown: null,
    hooks: [],
    gherkin: {
        features: './features/*.feature',
        steps: [
            './step_definitions/createPlaces.js',
            './step_definitions/addComment.js',

        ]
    },
    plugins: {
        screenshotOnFail: {
            enabled: true
        }
    },
    tests: './*_test.js',
    name: 'tests'
};
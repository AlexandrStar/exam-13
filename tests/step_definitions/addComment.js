const { I } = inject();

Given('я нохожусь на странице Авторизация', () => {
    I.amOnPage('/login')
});

When('я жму на картинку', () => {
    I.click(`//*[@id="root"]/div[2]/div/div[2]/div/div/a/img`);
});

When('я ввожу коментарий {string} в поле {string}', (text, fieldName) => {
    I.fillField({xpath: `//textarea[@id='${fieldName}']`}, text)
});

When('я выбераю рейтинг {string} в поле {string}', (num, optionName) => {
    I.selectOption(`//select[@name='${optionName}']`, num)
});

When('я нажимаю на {string}', (text) => {
    I.click(`//button[.='${text}']`)
});

Then('я вижу текст {string}', () => {
    I.waitForElement(`//*[@id="root"]/div[2]/div/div[3]/div[1]/div[2]/div/span`, 10)
});

const { I } = inject();

Given('я нохожусь на странице Авторизация', () => {
    I.amOnPage('/login')
});

When('я ввожу {string} в поле {string}', (text, fieldName) => {
    I.fillField({xpath: `//input[@id='${fieldName}']`}, text)
});

When('нажимаю на кнопку {string}', (text) => {
    I.click(`//button[.='${text}']`);
    I.wait(10);
});

When('я вижу оповещение {string}', (text) => {
    I.waitForElement(`//div[contains(@class, 'notification-container')]/span[.='${text}']`, 10);
});

When('я вижу имя пользователя {string}', (name) => {
    I.waitForElement(`//*[@id="root"]/header/nav/ul/li/a/text()[2]`, 10);
});

When('я выбераю кнопку {string}', (name) => {
    I.click(`//*[@id="root"]/header/nav/ul/li/a`);
    I.wait(2);
    I.click(`//a[.='${name}']`);
});

When('я ввожу описание {string} в поле {string}', (text, fieldName) => {
    I.fillField({xpath: `//textarea[@id='${fieldName}']`}, text)
});

When('я загружаю картинку', () => {
    I.attachFile({xpath:"//input[@id='image']"}, 'dataImage/burger.jpeg');
});

When('я подтверждаю что согласен с регистрацией', () => {
    I.click(`//input[@id='understand']`)
});

When('я нажимаю на {string}', (text) => {
    I.click(`//button[.='${text}']`)
});

Then('я вижу текст {string}', (text) => {
    I.waitForElement(`//p[contains(text(),'${text}')]`, 10)
});

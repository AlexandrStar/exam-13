import axios from '../../axios-api';
import {push} from "connected-react-router";

export const FETCH_PLACES_SUCCESS = 'FETCH_PLACES_SUCCESS';
export const FETCH_SINGLE_PLACE_SUCCESS = 'FETCH_SINGLE_PLACE_SUCCESS';
export const CREATE_PLACE_SUCCESS = 'CREATE_PLACE_SUCCESS';

export const fetchPlacesSuccess = places => ({type: FETCH_PLACES_SUCCESS, places});
export const fetchSinglePlaceSuccess = place => ({type: FETCH_SINGLE_PLACE_SUCCESS, place});
export const createPlaceSuccess = () => ({type: CREATE_PLACE_SUCCESS});

export const fetchPlaces = () => {
    return dispatch => {
        return axios.get('/places').then(
            response => dispatch(fetchPlacesSuccess(response.data))
        );
    };
};

export const fetchSinglePlace =(id)=>{
    return dispatch =>{
        return axios.get(`/places/${id}`).then(
            response => dispatch(fetchSinglePlaceSuccess(response.data))
        )
    };
};

export const createPlace = (placeData) => {
    return dispatch => {
        return axios.post('/places', placeData).then(
            response => {
                dispatch(createPlaceSuccess(response.data));

            }
        );
    };
};

export const createPhoto = (placeData, id, redirect = true) => {
    return dispatch => {
        return axios.post(`/places/${id}`, placeData).then(
            response => {
                dispatch(createPlaceSuccess(response.data));
                if (redirect) {
                    dispatch(push('/places/' + response.data._id));
                }
            },
            error => {

            }
        )
    }
};

export const deletePlace = id => {
    return (dispatch) => {
        return axios.delete(`/places/${id}`).then(
            () => {
                dispatch(fetchPlaces())
            }
        )
    }
};
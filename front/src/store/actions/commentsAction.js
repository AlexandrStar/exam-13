import axios from '../../axios-api';

export const FETCH_COMMENT_SUCCESS = 'FETCH_COMMENT_SUCCESS';

export const fetchCommentSuccess = comments => ({type: FETCH_COMMENT_SUCCESS, comments});

export const fetchComment = id => {
    return (dispatch) => {
        return axios.get(`/comments?placeId=${id}`).then(
            response => {
                dispatch(fetchCommentSuccess(response.data));
            }
        );
    };
};

export const createComment = (comment) => {
    return dispatch => {
        return axios.post('/comments', comment).then(
            (result) => {
                dispatch(fetchComment(comment.placeId))
            }
        );
    };
};

export const deleteComment = (id,comment) => {
    return (dispatch) => {
        return axios.delete(`/comments/${id}`).then(
            () => {
                dispatch(fetchComment(comment.placeId))
            }
        )
    }
};
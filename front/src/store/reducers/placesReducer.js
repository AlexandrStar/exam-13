import {FETCH_PLACES_SUCCESS, FETCH_SINGLE_PLACE_SUCCESS} from "../actions/placesAction";

const initialState = {
    places: [],
    singlePlace: null
};

const placesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PLACES_SUCCESS:
            return {...state, places: action.places};
        case FETCH_SINGLE_PLACE_SUCCESS:
            return {...state, singlePlace: action.place};
        default:
            return state;
    }
};

export default placesReducer;

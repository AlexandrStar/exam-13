import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody, CardImg, CardText, Col, Row} from "reactstrap";
import {deletePlace, fetchPlaces} from "../../store/actions/placesAction";
import {connect} from "react-redux";
import config from "../../config";
import {Link} from "react-router-dom";

class PlacesPage extends Component {

    componentDidMount() {
        this.props.onFetchPlaces();
    }

    render() {
        console.log(this.props.places);
        return (
            <Fragment>
                <Row>
                    {this.props && this.props.places.map((place, index) => (
                        <Col key={index} sm={3}>
                            <Card className="text-center" style={{marginBottom: '20px'}} >
                                <CardBody>
                                    <Link to={`/places/${place._id}`}>
                                        <CardImg top width="100%" src={config.apiURL + '/uploads/' + place.image} alt="place" />
                                        <CardText style={{paddingTop: '10px'}}>{place.title}</CardText>
                                    </Link>
                                    <CardText>
                                        <small className="text-muted">
                                            (
                                                {place && place.countComment ? place.countComment.count: 0} Reviews
                                            )
                                        </small>
                                    </CardText>
                                    <Col sm={2}>
                                        <Button
                                            style={{margin: '20px'}}
                                            color="danger"
                                            className="float-right"
                                            onClick={() => this.props.deletePlace(place._id)}
                                        >
                                            Remove
                                        </Button>
                                    </Col>
                                </CardBody>
                            </Card>
                        </Col>
                    ))}
                </Row>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    places: state.places.places,
});

const mapDispatchToProps = dispatch => ({
    onFetchPlaces: () => dispatch(fetchPlaces()),
    deletePlace: (id) => dispatch(deletePlace(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PlacesPage);
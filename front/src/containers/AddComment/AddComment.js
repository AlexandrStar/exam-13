import React, {Component} from 'react';
import {connect} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";
import {Button, Col, Form, Input, Label, Row} from "reactstrap";
import {createComment} from "../../store/actions/commentsAction";

const ratingNumber = [
    '1.0', '1.1', '1.2', '1.3', '1.4', '1.5', '1.6','1.7','1.8', '1.9',
    '2.0', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6','2.7','2.8', '2.9',
    '3.0', '3.1', '3.2', '3.3', '3.4', '3.5', '3.6','3.7','3.8', '3.9',
    '4.0', '4.1', '4.2', '4.3', '4.4', '4.5', '4.6','4.7','4.8', '4.9',
    '5.0',
];

class AddComment extends Component {
    state = {
        commentText: '',
        qualityKitchen: '',
        qualityService: '',
        atmosphere: '',
        placeId: this.props.placeId
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.createComment({...this.state})

    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <hr/>
                <FormElement
                    type="textarea"
                    propertyName="commentText"
                    title="Add a comment:"
                    onChange={this.inputChangeHandler}
                />

                <Row>
                    <Col sm={3}>
                        <Label>Quality of the kitchen (bar) </Label>
                        <Input
                            type="select"
                            name="qualityKitchen"
                            value={this.state.qualityKitchen}

                            onChange={this.inputChangeHandler}

                        >
                            {ratingNumber.map(number => (
                                <option key={number} value={number}>{number}</option>
                            ))}
                        </Input>
                    </Col>
                    <Col sm={3}>
                        <Label>Quality of service </Label>
                        <Input
                            type="select"
                            name="qualityService"
                            value={this.state.qualityService}
                            onChange={this.inputChangeHandler}
                        >
                            {ratingNumber.map(number => (
                                <option key={number} value={number}>{number}</option>
                            ))}
                        </Input>
                    </Col>
                    <Col sm={3}>
                        <Label>Atmosphere </Label>

                        <Input
                            type="select"
                            name="atmosphere"
                            onChange={this.inputChangeHandler}
                            value={this.state.atmosphere}

                        >
                            {ratingNumber.map(number => (
                                <option key={number} value={number}>{number}</option>
                            ))}
                        </Input>
                    </Col>
                    <Col sm={3} className="p-4 mt-1">
                        <Button type="submit" color="primary">
                            Add
                        </Button>
                    </Col>
                </Row>
            </Form>
        );
    }
}
const mapDispatchToProps = dispatch =>({
    createComment: data => dispatch(createComment(data)),
});

export default connect(null,mapDispatchToProps)(AddComment);
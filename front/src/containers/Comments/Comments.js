import React, {Component, Fragment} from 'react';
import StarRatings from "react-star-ratings";

import {connect} from "react-redux";
import {Button, Card, CardText, CardTitle, Col, Row} from "reactstrap";
import {deleteComment, fetchComment} from "../../store/actions/commentsAction";
import AverageRatings from "../../components/AverageRatings/AverageRatings";

class Comments extends Component {

    componentDidMount() {
        this.props.fetchComments(this.props.placeId)
    }

    render() {
        let numberOverall = 0;
        let numberKitchen = 0;
        let numberService = 0;
        let numberAtmosphere = 0;

        {this.props.comments.map(comment => {
            numberKitchen += comment.qualityKitchen / this.props.comments.length;
            numberService += comment.qualityService / this.props.comments.length;
            numberAtmosphere += comment.atmosphere / this.props.comments.length;
            numberOverall = (numberKitchen + numberService + numberAtmosphere) / 3
        })}

        return (
            <Fragment>
                <AverageRatings
                    numberOverall={parseFloat(numberOverall.toFixed(1))}
                    numberKitchen={parseFloat(numberKitchen.toFixed(1))}
                    numberService={parseFloat(numberService.toFixed(1))}
                    numberAtmosphere={parseFloat(numberAtmosphere.toFixed(1))}
                />

                <hr/>
                <h3>Comments</h3>
                {this.props.comments.length > 0 ? this.props.comments.map(comment => (
                    <Card key={comment._id} className="mt-2 p-3">
                        <CardTitle>
                            On {comment.dateTime}, <span>{comment.user.username}:</span>
                        </CardTitle>
                        <CardText>{comment.commentText}</CardText>

                        <Row>
                            <Col sm={2}>
                                Quality of the kitchen (bar):
                            </Col>

                            <Col sm={10}>
                                <StarRatings
                                    rating={comment.qualityKitchen}
                                    ratingToShow={comment.qualityKitchen}
                                    starDimension={'25px'}
                                    starRatedColor="orange"
                                    numberOfStars={5}
                                />

                                <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {comment.qualityKitchen}
                             </span>
                            </Col>
                            <Col sm={2}>
                                Quality of service:
                            </Col>

                            <Col sm={10}>
                                <StarRatings
                                    rating={comment.qualityService}
                                    ratingToShow={comment.qualityService}
                                    starDimension={'25px'}
                                    starRatedColor="orange"
                                    numberOfStars={5}
                                />

                                <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {comment.qualityService}
                             </span>
                            </Col>
                            <Col sm={2}>
                                Atmosphere:
                            </Col>
                            <Col sm={10}>
                                <StarRatings
                                    rating={comment.atmosphere}
                                    ratingToShow={comment.atmosphere}
                                    starDimension={'25px'}
                                    starRatedColor="orange"
                                    numberOfStars={5}
                                />

                                <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {comment.atmosphere}
                             </span>
                            </Col>
                            {(this.props.user._id === comment.user._id) && this.props.user.role === 'admin' ?
                                <Col sm={2}>
                                    <Button
                                        style={{margin: '20px'}}
                                        color="danger"
                                        className="float-right"
                                        onClick={() => this.props.deleteComment(comment._id, comment)}
                                    >
                                        Remove
                                    </Button>
                                </Col>
                                :null}
                        </Row>
                    </Card>
                )): <div>No Comments</div>}

            </Fragment>

        );
    }
}


const mapStateToProps = state => ({
    comments: state.comments.comments,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchComments: placeId => dispatch(fetchComment(placeId)),
    deleteComment: (id,comment) => dispatch(deleteComment(id,comment)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Comments);
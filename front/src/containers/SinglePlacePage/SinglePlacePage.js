import React, {Component} from 'react';
import {connect} from "react-redux";
import {createPhoto, fetchSinglePlace} from "../../store/actions/placesAction";
import config from "../../config";
import {Button, Card, CardImg, CardText, CardTitle, Col, Form, FormGroup, Input, Label, Row, Spinner} from "reactstrap";
import AddComment from "../AddComment/AddComment";
import Comments from "../Comments/Comments";
import Slider from "react-slick";
import "./SlickCarousel.css";
import "./SinglePlacePage.css";

class SinglePlacePage extends Component {
    state={
        gallery: [],
    };

    componentDidMount() {
        this.props.onFetchSinglePlace(this.props.match.params.id)
    }

    galleryChangeHandler = event => {
        event.preventDefault();

        let files = Array.from(event.target.files);

        files.forEach((file) => {
            let reader = new FileReader();
            reader.onloadend = () => {
                this.setState({
                    gallery: [...this.state.gallery, file],
                });
            };
            reader.readAsDataURL(file);
        });
    };

    submitFormHandler = event => {
        event.preventDefault();
        const formData = new FormData();

        for (let i = 0; i < this.state.gallery.length; i++) {
            formData.append('gallery', this.state.gallery[i]);
        }

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.createPhoto(formData, this.props.match.params.id);
    };

    render() {
        if (this.props.place === null) return <Spinner color="success" />;

        const settings = {
            dots: true,
            infinite: false,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1
        };

        return (
            <div>
                <Card className="border-0">
                    <Row>
                        <Col sm={8}>
                            <CardTitle><h1>{this.props.place.title}</h1></CardTitle>
                            <CardText>{this.props.place.description}</CardText>
                        </Col>
                        <Col sm={4}>
                            <CardImg
                                src={config.apiURL + '/uploads/' + this.props.place.image}
                                alt={this.props.place.title}
                            />
                        </Col>
                    </Row>
                </Card>
                <hr/>
                <div className="gallery-slider">
                    <h5>Gallery</h5>
                    <Slider {...settings}>
                        {
                            this.props.place.gallery.map(slider => (
                                <img key={slider} className='Slide-img' src={config.apiURL + '/uploads/' + slider} alt={this.props.place.name}/>
                            ))
                        }
                    </Slider>
                </div>
                <div>
                    <Comments placeId={this.props.match.params.id}/>
                    {this.props.user ?
                        <AddComment placeId={this.props.match.params.id}/>
                        : null}
                </div>
                <hr/>
                <div>
                    {this.props.user ?
                        <Form onSubmit={this.submitFormHandler}>
                            <FormGroup row>
                                <Label for="gallery" sm={2}>Галерея</Label>
                                <Col sm={10}>
                                    <Input
                                        type="file"
                                        name="gallery" id="gallery"
                                        onChange={this.galleryChangeHandler}
                                        multiple
                                    />
                                </Col>
                            </FormGroup>

                            <FormGroup row>
                                <Col sm={{offset:2, size: 10}}>
                                    <Button type="submit" color="primary">Add Photo</Button>
                                </Col>
                            </FormGroup>
                        </Form>
                    : null}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    place: state.singlePlace.singlePlace,
    user: state.users.user,
    comments: state.comments.comments,
});

const mapDispatchToProps = dispatch => ({
    onFetchSinglePlace: (id) => dispatch(fetchSinglePlace(id)),
    createPhoto: (placeData, id) => dispatch(createPhoto(placeData, id))
});

export default connect(mapStateToProps, mapDispatchToProps)(SinglePlacePage);
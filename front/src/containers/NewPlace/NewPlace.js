import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import PlaceAddForm from "../../components/PlaceAddForm/PlaceAddForm";
import {createPlace} from "../../store/actions/placesAction";


class NewPlace extends Component {

    createPlace = placeData => {
        this.props.onPlaceCreated(placeData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                {this.props.user ?
                    <div>
                        <h2>Add new place</h2>
                        <PlaceAddForm
                            onSubmit={this.createPlace}
                        />
                    </div> : <h3>Зарегистрируетесь пожалуйста!</h3>}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    onPlaceCreated: placeData => dispatch(createPlace(placeData)),

});

export default connect(mapStateToProps, mapDispatchToProps)(NewPlace);

import React, {Fragment} from 'react';
import StarRatings from "react-star-ratings";
import {Col, Row} from "reactstrap";
import OverallRating from "./OverallRatings";


const AverageRatings = (props) => {
    return (
        <Fragment>
            <hr/>
            <Row>
                <Col sm={2}>
                    Overall
                </Col>
                <Col sm={10}>
                    <OverallRating numberOverall={props.numberOverall}/>
                </Col>
                <Col sm={2}>
                    Quality of the kitchen (bar)
                </Col>
                <Col sm={10}>
                    <StarRatings
                        rating={props.numberKitchen}
                        ratingToShow={props.numberKitchen}
                        starDimension={'25px'}
                        starRatedColor="orange"
                        numberOfStars={5}
                    />
                    <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.numberKitchen}
                </span>
                </Col>

                <Col sm={2}>
                    Quality of service
                </Col>
                <Col sm={10}>
                    <StarRatings
                        rating={props.numberService}
                        ratingToShow={props.numberService}
                        starDimension={'25px'}
                        starRatedColor="orange"
                        numberOfStars={5}
                    />
                    <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.numberService}
                </span>
                </Col>
                <Col sm={2}>
                    Atmosphere
                </Col>
                <Col sm={10}>
                    <StarRatings
                        rating={props.numberAtmosphere}
                        ratingToShow={props.numberAtmosphere}
                        starDimension={'25px'}
                        starRatedColor="orange"
                        numberOfStars={5}
                    />
                    <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.numberAtmosphere}
                </span>
                </Col>
            </Row>
        </Fragment>


    );
};

export default AverageRatings;
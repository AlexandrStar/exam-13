import React from 'react';
import StarRatings from "react-star-ratings";

const OverallRating = (props) => {
    return (
        <div>
            <StarRatings
                rating={props.numberOverall}
                ratingToShow={props.numberOverall}
                starDimension={'25px'}
                starRatedColor="orange"
                numberOfStars={5}
            />
            <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.numberOverall}
                </span>
        </div>
    );
};

export default OverallRating;
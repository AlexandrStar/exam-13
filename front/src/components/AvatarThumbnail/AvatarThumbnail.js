import React from 'react';
import config from '../../config'

import avatarnNotAvailable from '../../assets/images/avatar_not _available.png';

const styles = {
    width: '50px',
    height: '50px',
    borderRadius: '50%',
    display: 'inline-block'
};

const AvatarThumbnail = (props) => {

    const src = props.user.facebookId ? props.user.avatarImage : config.apiURL +  '/uploads/' + props.user.avatarImage;

    let image = avatarnNotAvailable;

    if (props.user.avatarImage) {
        image =  src
    }


    return <img src={image} style={styles} className="img-thumbnail" alt="avatar"/>;
};

export default AvatarThumbnail;

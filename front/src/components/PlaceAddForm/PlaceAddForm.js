import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";

class PlaceAddForm extends Component {
    state = {
        title: '',
        image:'',
        description: '',
        understand: false,
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    checkBoxHandler = (event) =>{
        this.setState({understand: event.target.checked})
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName="title"
                    title="Title"
                    type="text" required
                    onChange={this.inputChangeHandler}
                    value={this.state.title}
                />

                <FormElement
                    propertyName="description"
                    title="Description"
                    type="textarea" required
                    value={this.props.description}
                    onChange={this.inputChangeHandler}
                />

                <FormElement
                    propertyName="image"
                    title="Place Image"
                    type="file" required
                    onChange={this.fileChangeHandler}
                />

                <FormGroup row>
                    <Label for="understand" sm={3}>
                        By sumbitting this from, you agree that the following information will be submitted to the
                        public domain, and administrators of this site will have full control over the said information.
                    </Label>
                    <Col sm={{size: 9}}>
                        <FormGroup check>
                            <Label check>
                                <Input type="checkbox" required name="understand" id="understand" onChange={this.checkBoxHandler} />{' '}
                                I understand
                            </Label>
                        </FormGroup>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col sm={{offset:3, size: 9}}>
                        <Button type="submit" color="primary">Submit new place</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default PlaceAddForm;
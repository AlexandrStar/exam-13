import React, {Fragment} from 'react';
import {NavLink as RouterNavLink} from 'react-router-dom';
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import AvatarThumbnail from "../../../AvatarThumbnail/AvatarThumbnail";

const UserMenu = ({user, logout}) => {
    return (
        <Fragment>
            <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret className='d-inline-block'>
                    Hello, {user.displayName}!
                </DropdownToggle>
                <AvatarThumbnail user={user}/>
                <DropdownMenu right>
                    <DropdownItem tag={RouterNavLink} to="/new_place" exact>
                        Add place
                    </DropdownItem>
                    <DropdownItem divider/>
                    <DropdownItem onClick={logout}>
                        Exit
                    </DropdownItem>
                </DropdownMenu>
            </UncontrolledDropdown>
        </Fragment>
    );
};

export default UserMenu;

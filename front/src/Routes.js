import React from 'react';
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import PlacesPage from "./containers/PlacesPage/PlacesPage";
import NewPlace from "./containers/NewPlace/NewPlace";
import SinglePlacePage from "./containers/SinglePlacePage/SinglePlacePage";

const Routes = () => {
    return (
        <Switch>
            <Route path="/" exact component={PlacesPage}/>
            <Route path="/places/:id" exact component={SinglePlacePage}/>
            <Route path="/new_place" exact component={NewPlace}/>
            <Route path="/register" exact component={Register} />
            <Route path="/login" exact component={Login} />
        </Switch>
    );
};

export default Routes;
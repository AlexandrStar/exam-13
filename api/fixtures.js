const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const User = require('./models/User');
const Place = require('./models/Place');
const Comment = require('./models/Comment');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const user = await User.create(
        {
            username: 'admin',
            password: '123',
            token: nanoid(),
            displayName: 'SuperAdmin',
            avatarImage:'admin.jpg',
            role: 'admin',
        },
        {
            username: 'user',
            password: '123',
            token: nanoid(),
            displayName: 'Anonymous',
            avatarImage:'noavatar.jpg',
        },
        {
            username: 'john',
            password: '123',
            token: nanoid(),
            displayName: 'John Doe',
            avatarImage:'noavatar.jpg',
        },
    );

    const place = await Place.create(
        {
            user: user[0]._id,
            title: 'MADFOOD',
            image: 'madfood.png',
            description: 'Средний чек 270c , Азиатская кухня, Восточная кухня, Китайская кухня ,Паназиатская кухня, Халяльная ' +
                'кухня, Бизнес-ланч от 130 сом, Бизнес-ланч, Бургерные, Заказ навынос, Некурящий зал'
        },
        {
            user: user[0]._id,
            title: 'KFC',
            image: 'kfc.png',
            description: 'Ресторан / Кафе: Средний чек 350c, Американская кухня, Бургерные, Заказ навынос, Некурящий зал'
        },
        {
            user: user[1]._id,
            title: 'Империя пиццы',
            image: 'imperpizza.png',
            description: 'Доставка готовых блюд: Горячие вторые блюда, Десерты, Пицца, Суши, Корпоративные обеды, ' +
                'Салаты, Супы, Еда в коробочках. Ресторан / Кафе: Средний чек 500c, Азиатская кухня, Европейская кухня, ' +
                'Итальянская кухня, Японская кухня, Заказ навынос, Завтрак, Кальян.'
        },
        {
            user: user[2]._id,
            title: 'Барашек',
            image: 'barashek.png',
            description: 'Кейтеринг: Банкет, Детские праздники, Кофе-брейк, Напитки, Фуршет, Шведский стол.' +
                'Ресторан / Кафе: Авторская кухня, Азиатская кухня, Восточная кухня, Европейская кухня,' +
                ' Киргизская кухня, Японская кухня, Винная карта, Детское меню, Заказ навынос, Живая музыка, ' +
                'Заказ столиков, Поминальные обеды, VIP-зал, Детский уголок, Проведение банкетов, Летняя терраса, ' +
                'Настольные игры, Развлечения для детей, Танцпол, Некурящий зал. Банкетные залы До 120 человек'
        }
    );

    await Comment.create(
        {
            user: user[0]._id,
            placeId: place[0]._id,
            commentText: 'Good',
            dateTime: '17.08.19 10:55',
            qualityKitchen: 4.5,
            qualityService: 4.3,
            atmosphere: 4.8
        },
        {
            user: user[1]._id,
            placeId: place[0]._id,
            commentText: 'Bad',
            dateTime: '17.08.19 11:55',
            qualityKitchen: 2.5,
            qualityService: 2.3,
            atmosphere: 2.8
        },
        {
            user: user[2]._id,
            placeId: place[0]._id,
            commentText: 'No bad',
            dateTime: '17.08.19 12:55',
            qualityKitchen: 3.5,
            qualityService: 3.3,
            atmosphere: 3.8
        },
        {
            user: user[0]._id,
            placeId: place[0]._id,
            commentText: 'Very Good',
            dateTime: '19.08.19 13:55',
            qualityKitchen: 4.9,
            qualityService: 5.0,
            atmosphere: 5.0
        },
        {
            user: user[0]._id,
            placeId: place[1]._id,
            commentText: 'Good',
            dateTime: '17.08.19 10:55',
            qualityKitchen: 4.5,
            qualityService: 4.3,
            atmosphere: 4.8
        },
        {
            user: user[1]._id,
            placeId: place[3]._id,
            commentText: 'Good',
            dateTime: '17.08.19 10:55',
            qualityKitchen: 4.5,
            qualityService: 4.3,
            atmosphere: 4.8
        },
        {
            user: user[2]._id,
            placeId: place[2]._id,
            commentText: 'Good',
            dateTime: '17.08.19 10:55',
            qualityKitchen: 4.5,
            qualityService: 4.3,
            atmosphere: 4.8
        },

    );

    await connection.close();
};

run().catch(error => {
    console.log('Something went wrong', error);
});
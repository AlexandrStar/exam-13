const path = require('path');

const rootPath = __dirname;

const dbUrl = process.env.NODE_ENV === 'test' ? 'mongodb://localhost/places_test': 'mongodb://localhost/places';

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    dbUrl,
    mongoOptions: {
        useNewUrlParser: true,
        useCreateIndex: true
    },
    facebook: {
        appId: '2114459662013499',
        appSecret: '658e53e19f4af212ef2ee7ea0b096760'
    }
};

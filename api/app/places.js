const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const auth = require('../middleware/auth');
const addAuth = require('../middleware/addAuth');
const permit = require('../middleware/permit');
const Place = require('../models/Place');
const Comment = require('../models/Comment');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', addAuth, async (req, res) => {
    try {
        const places = await Place.find().sort('title');

        const commentsCount = await Comment.aggregate([{$group: {_id: '$placeId', count: {$sum: 1}}}]);

        const placesWithCount = places.map(place => {
            const placeCount = commentsCount.find(count => count._id.equals(place._id));
            return {
                ...place.toObject(),
                countComment: placeCount
            }
        });

        return res.send(placesWithCount)
    }catch (e) {
        return res.status(500).send(e);
    }
});

router.get('/:id', addAuth, async (req, res) => {
    try {
        const place = await Place.findById(req.params.id);
        res.send(place);
    }catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', [auth, permit('user', 'admin')], upload.single('image'), (req, res) => {
    try {
        const placeData = req.body;

        if (placeData.understand === 'false') {
            res.sendStatus(403);
        } else {
        if (req.file) {
            placeData.image = req.file.filename;
        }

        placeData.user = req.user._id;

        const place = new Place(placeData);

        place.save();

        return res.send(place)
        }
    }catch (e) {
        return res.status(500).send(e);
    }
});

router.post('/:id', auth, upload.fields([{name: 'gallery', maxCount: 10}]), async (req, res) => {
    try {
        const placeData = await Place.findById(req.params.id);

        if (req.files) {
            placeData.gallery = req.files.gallery ? req.files.gallery.map(file =>  file.filename) : undefined;
        }

        placeData.user = req.user._id;

        const place = new Place(placeData);
        await place.save();

        return res.send(place);
    } catch (e) {
        console.log(e);
        return res.sendStatus(500);
    }
});

router.delete('/:id', [auth, permit('admin')],async (req,res)=>{
    try {
        await Place.findByIdAndDelete({_id: req.params.id});
        res.send('ok');

        await Comment.findByIdAndDelete({userId:req.params.id});
        res.send('ok');

    }catch (e) {
        return res.status(500)
    }
});

module.exports = router;